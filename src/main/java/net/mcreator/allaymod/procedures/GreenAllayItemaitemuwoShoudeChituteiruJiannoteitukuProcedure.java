package net.mcreator.allaymod.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.allaymod.item.GreenAllayItemItem;
import net.mcreator.allaymod.AllayModMod;

import java.util.Map;

public class GreenAllayItemaitemuwoShoudeChituteiruJiannoteitukuProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				AllayModMod.LOGGER.warn("Failed to load dependency world for procedure GreenAllayItemaitemuwoShoudeChituteiruJiannoteituku!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				AllayModMod.LOGGER.warn("Failed to load dependency x for procedure GreenAllayItemaitemuwoShoudeChituteiruJiannoteituku!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				AllayModMod.LOGGER.warn("Failed to load dependency y for procedure GreenAllayItemaitemuwoShoudeChituteiruJiannoteituku!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				AllayModMod.LOGGER.warn("Failed to load dependency z for procedure GreenAllayItemaitemuwoShoudeChituteiruJiannoteituku!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				AllayModMod.LOGGER.warn("Failed to load dependency entity for procedure GreenAllayItemaitemuwoShoudeChituteiruJiannoteituku!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY)
				.getItem() == GreenAllayItemItem.block) {
			if (entity instanceof PlayerEntity) {
				((PlayerEntity) entity).abilities.isFlying = (true);
				((PlayerEntity) entity).sendPlayerAbilities();
			}
			world.addParticle(ParticleTypes.EFFECT, x, y, z, 0, 0, 0);
			world.addParticle(ParticleTypes.EFFECT, (x + 0.5), y, (z + 0.5), 0, 0, 0);
			world.addParticle(ParticleTypes.EFFECT, (x - 0.5), y, (z - 0.5), 0, 0, 0);
		} else {
			if (entity instanceof PlayerEntity) {
				((PlayerEntity) entity).abilities.isFlying = (false);
				((PlayerEntity) entity).sendPlayerAbilities();
			}
		}
	}
}
